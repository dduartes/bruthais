@extends('layouts.main')
@section('title','Novo Episdio')
@section('conteudo')
    @if(count($errors))
        @include('layouts.errors')
        <hr>
    @endif
    <form action="/episodios" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="textoDescricaoProjeto">Projeto</label>
            {{ Form::select('projeto_id', $projetos,null,['class' => 'form-control','placeholder' => 'Informe o Projeto...']) }}
        </div>

        <div class="form-group">
            <label for="inputNomeEpisodio">Título do Episódio</label>
            <input type="text" class="form-control" id="inputNomeEpisodio" name="titulo" placeholder="Nome do Episódio"
                   required>
        </div>

        <div class="form-group">
            <label for="inputSubTituloEpisodio">Subtítulo do Episódio</label>
            <input type="text" class="form-control" id="inputSubTituloEpisodio" name="subtitulo" placeholder="Subtítulo">
        </div>

        <div class="form-group">
            <label for="inputNumeroDoEpisodio">Número do Episódio</label>
            <input type="number" class="form-control" id="inputNumeroDoEpisodio" name="numero" placeholder="Número"
                   required>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="finalizado"> Finalizado
            </label>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-warning">Salvar</button>
            </div>
        </div>
    </form>
@endsection
