@extends('layouts.main')
@section('title','Registro de Usuário')
@section('conteudo')
    <div class="col-sm-8">

        <h1> Registro de Usuário </h1>

        <form action="/admin/registrar" method="post">
            {{csrf_field()}}

            <div class="form-group">
                <label for="nickInput">Nick:</label>
                <input type="text" class="form-control" id="nickInput" name="name" required>
            </div>

            <div class="form-group">
                <label for="emailInput">Email:</label>
                <input type="email" class="form-control" id="emailInput" name="email" required>
            </div>

            <div class="form-group">
                <label for="passwordInput">Senha:</label>
                <input type="password" class="form-control" id="passwordInput" name="password" required>
            </div>
            <div class="form-group">
                <label for="passwordInput_confirmation">Repita sua Senha:</label>
                <input type="password" class="form-control" id="passwordInput_confirmation" name="password_confirmation"
                       required>
            </div>
            <div class="form-group">

                <button type="submit" class="btn btn-warning">Enviar</button>

            </div>
            <div class="form-group">

                @include('layouts.errors')

            </div>

        </form>
    </div>

@endsection