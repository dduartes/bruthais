@extends('layouts.main')
@section('title','Login Administração')
@section('conteudo')
    <div class="col-md-8">

        <div>
            <h1>Admin Login</h1>
        </div>

        <form method="post" action="/admin/login">
            {{csrf_field()}}

            <div class="form-group">
                <label for="emailInput">Email:</label>
                <input type="email" class="form-control" id="emailInput" name="email">
            </div>

            <div class="form-group">
                <label for="passInput">Password:</label>
                <input type="password" class="form-control" id="passInput" name="password">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-warning">Logar</button>
            </div>
        </form>

    </div>
@endsection