@extends('layouts.main')
@section('title','Novo Projeto')
@section('conteudo')
    @if(count($errors))
        @include('layouts.errors')
        <hr>
    @endif
    <form action="/projetos" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="inputNomeProjeto">Nome</label>
            <input type="text" class="form-control" id="inputNomeProjeto" name="nome" placeholder="Nome do Projeto"
                   required>
        </div>

        <div class="form-group">
            <label for="textoDescricaoProjeto">Descrição do Projeto</label>
            <textarea id="textoDescricaoProjeto" class="form-control" rows="3" name="descricao" required></textarea>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="finalizado"> Finalizado
            </label>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-warning">Salvar</button>
            </div>
        </div>
    </form>

@endsection
