@extends('layouts.main')
@section('title','Projetos')
@section('conteudo')
    <div class="container">
        <!-- Example row of columns -->
        <div class="row top7">
            @foreach($projetos as $projeto)
                <div class="col-md-3 top7">
                    <div class="epContainerBox boxRound epiContainerSombra">
                        <div class="epInf">
                            <img border="0" width="166" height="96" alt="Screen {{$projeto->id}}" class="epiSS boxRound"
                                 src="">
                            <br> {{$projeto->nome}}
                            <br> Episódios: {{sizeof($projeto->episodios)}}/??
                            <br> Último Lançamento: {{$projeto->dtUltimoEp()}}
                            <br> Concluído: {{$projeto->finalizado}}<br>
                            <hr/>
                            <a href="/projetos/{{$projeto->id}}" class="btn btn-warning">Ver Episódios</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <hr>
        {{-- Mock de botoes para criar/editar --}}

    </div>
    <div class="container">
        <a href="/projetos/novo">Novo Projeto</a>
    </div>
@endsection