@extends('layouts.main')
@section('title')
    {{$projeto->nome}}
@endsection
@section('conteudo')
    <div class="container">
        <div class="card">
            <strong>
                {{$projeto->nome}}
            </strong>
            <div class="card-block">
                {{$projeto->descricao}}
            </div>
        </div>
        <div class="row top7">
            @foreach($projeto->episodios as $episodio)
                <div class="col-sm-4 top7">
                    <div class="epContainerBox boxRound epiContainerSombra">
                        <div class="epInf">
                            <img border="0" width="166" height="96" alt="Screen {{$episodio->id}}"
                                 class="epiSS boxRound"
                                 src="">
                            <br> <strong>{{$episodio->numero}}</strong>
                            <br> {{$episodio->titulo}}
                            @if($episodio->subtitulo)
                                <br> {{$episodio->subtitulo}}
                            @endif
                            <br> Data do Lançamento: {{$episodio->created_at->format('d/m/Y')}}
                            <hr/>
                            <div class="row top7">
                                @foreach($episodio->links as $link)
                                    <div class="col-sm-4 top7">
                                        <a href="{{$link->url}}" class="btn btn-warning">{{$link->mirror->nome}}</a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <hr>
        {{-- Mock de botoes para criar/editar --}}

    </div>
    <div class="container">
        <a href="/projetos/novo">Novo Projeto</a>
    </div>
@endsection