<div id="cabecalhotopo">
    <img id="logo" src="/css/logo.png"/>
    <div id="buscatopo">
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Busca">
            <button class="btn btn-warning my-2 my-sm-0" type="submit">Buscar</button>
        </form>

        @if(Auth::check())
            <div class="ml-auto">
                Bem Vindo, {{Auth::user()->name}} - <a href="/logout">Logout</a>
            </div>
        @else
            <div class="ml-auto">
                <a href="/login">Admin Login</a>
            </div>
        @endif

    </div>
    <img id="impact" src="/css/impact.png"/>
    <img id="seriedestaque" src="/css/seriedestaque01.png"/>
</div>
<div id="menutopo">
    <div class="menu">
        @include("layouts.nav")
    </div>
</div>
<hr/>