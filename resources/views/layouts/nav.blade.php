<nav class="navbar navbar-toggleable-md navbar-inverse">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="/">Home</a>
        </li>
      <!--  <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="/projetos" id="dropdown01"
               data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">Projetos</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
                <a class="dropdown-item" href="#">Projeto 1</a>
                <a class="dropdown-item" href="#">Projeto 2</a>
                <a class="dropdown-item" href="#">Projeto 3</a>
            </div>
        </li>-->
        <li class="nav-item">
            <a class="nav-link" href="/projetos">Projetos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Equipe</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="#">Doações (Indisponível)</a>
        </li>
    </ul>
</nav>