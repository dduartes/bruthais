<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projeto extends Model
{
    protected $guarded = [];

    public function episodios()
    {
        return $this->hasMany(Episodio::class);
    }

    public function dtUltimoEp(){
        $eps = $this->episodios;
        if(sizeof($eps)>0){
            return $eps->last()->created_at->format('d/m/Y');
        }
        return "";
    }

    public function adicionarEpisodio(Episodio $episodio){
        $this->episodios()->save($episodio);
    }
}
