<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    public function episodio()
    {
        return $this->belongsTo(Episodio::class);
    }
    public function mirror(){
        return $this->belongsTo(Mirror::class);
    }
}
