<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth')->except(['loginPage','salvarNovo','registrar','login']);
    }

    public function registrar()
    {
        return view('admin.registrar');
    }

    public function salvarNovo(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'required|confirmed'
        ]);
        $user = User::create(request(['name', 'email', 'password']));

        auth()->login($user);

        return redirect()->home();

    }

    public function loginPage()
    {
        return view('admin.login');
    }
    public function login(){

        if (!auth()->attempt(request(['email','password']))) {
            redirect()->back();
        }
        return redirect()->intended('welcome');
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->home();

    }
}
