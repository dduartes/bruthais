<?php

namespace App\Http\Controllers;

use App\Episodio;
use App\Projeto;
use Illuminate\Http\Request;

class EpisodioController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth')->except(['index', 'show']);


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->home();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projetos = Projeto::pluck('nome', 'id')->all();
        return view('episodio.novo', compact('projetos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required|min:3',
            'projeto_id' => 'required',
        ]);
        $projeto = Projeto::find($request->get('projeto_id'));

        // dd($projeto);
        $ep = new Episodio(request(['projeto_id', 'titulo', 'subtitulo', 'numero']));
        $projeto->adicionarEpisodio($ep);
        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Episodio $episodio
     * @return \Illuminate\Http\Response
     */
    public function show(Episodio $episodio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Episodio $episodio
     * @return \Illuminate\Http\Response
     */
    public function edit(Episodio $episodio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Episodio $episodio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Episodio $episodio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Episodio $episodio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Episodio $episodio)
    {
        //
    }
}
