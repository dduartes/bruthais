<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Episodio extends Model
{
    protected $guarded = [];

    public function projeto()
    {
        return $this->belongsTo(Projeto::class);
    }

    public function links()
    {
        return $this->hasMany(Link::class);
    }

    public function adicionarLink(Link $link){
        $this->links()->save($link);
    }
}
