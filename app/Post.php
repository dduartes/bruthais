<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function episodios(){
        
        return $this->hasMany(Episodio::class);
    }
}
