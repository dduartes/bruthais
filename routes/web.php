<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name("home");

Route::get("/login", "AdminController@loginPage");
Route::get('/projetos', 'ProjetoController@index');
Route::get('/projetos/novo', 'ProjetoController@create');
Route::post('/projetos', 'ProjetoController@store');
Route::get('/projetos/{projeto}', 'ProjetoController@show');

Route::get('/episodios/novo', 'EpisodioController@create');
Route::post('/episodios', 'EpisodioController@store');

Route::get('/admin/registrar','AdminController@registrar');
Route::post('/admin/registrar','AdminController@salvarNovo');
Route::get('logout', 'Auth\LoginController@logout');

Auth::routes();

Route::get('/home', 'HomeController@index');

?>
