<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Episodio
 *
 */
	class Episodio extends \Eloquent {}
}

namespace App{
/**
 * App\Link
 *
 */
	class Link extends \Eloquent {}
}

namespace App{
/**
 * App\Mirror
 *
 */
	class Mirror extends \Eloquent {}
}

namespace App{
/**
 * App\Post
 *
 */
	class Post extends \Eloquent {}
}

namespace App{
/**
 * App\Projeto
 *
 */
	class Projeto extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
	class User extends \Eloquent {}
}

